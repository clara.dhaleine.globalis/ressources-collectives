# Intro to React FC + TypeScript

## Resources

- [React TypeScript cheatsheet](https://github.com/typescript-cheatsheets/react-typescript-cheatsheet)
- [Boostrap a React + TypeScript project](https://github.com/basarat/typescript-react/tree/master/01%20bootstrap)

- [TypeScript type’s notation](https://2ality.com/2018/04/type-notation-typescript.html)
- [Official TypeScript types doc](https://www.typescriptlang.org/docs/handbook/basic-types.html)
- [TypeScript playground](http://www.typescriptlang.org/play/)
- [React doc TypeScript section](https://reactjs.org/docs/static-type-checking.html#typescript)
- [Using the State hook](https://reactjs.org/docs/hooks-state.html)
- [Function vs. Class-Components in React](https://medium.com/@Zwenza/functional-vs-class-components-in-react-231e3fbd7108)
- [TypeScript Tutorial For Beginners](https://www.valentinog.com/blog/typescript/)
- [A Beginner’s Guide to using TypeScript with React](https://dev.to/bmcmahen/a-beginners-guide-to-using-typescript-with-react-7m6)
- [Simplifying React Forms with Hooks](https://rangle.io/blog/simplifying-controlled-inputs-with-hooks/)
- [Code-Splitting](https://reactjs.org/docs/code-splitting.html#reactlazy)